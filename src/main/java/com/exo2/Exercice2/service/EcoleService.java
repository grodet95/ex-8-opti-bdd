package com.exo2.Exercice2.service;

import com.exo2.Exercice2.dto.EcoleDto;
import com.exo2.Exercice2.entity.Ecole;
import com.exo2.Exercice2.entity.Etudiant;
import com.exo2.Exercice2.mapper.EcoleMapper;
import com.exo2.Exercice2.repository.EcoleRepository;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class EcoleService {
    private EcoleRepository ecoleRepository;
    private EcoleMapper ecoleMapper;

    @Cacheable(value = "ecolesList")
    public Page<EcoleDto> findAll(Pageable pageable) {
        return ecoleRepository.findAll(pageable).map(ecoleMapper::toDto);
    }

    @Cacheable(value = "ecoles", key = "#id")
    public EcoleDto findById(long id) {
        return ecoleMapper.toDto(ecoleRepository.findById(id).orElse(null));
    }

    @Cacheable(value = "ecolesByNomEtudiant", key = "#nomEtudiant")
    public Page<EcoleDto> findByNomEtudiant(String nomEtudiant, Pageable pageable) {
        return ecoleRepository.findEcolesFromNomEtudiant(nomEtudiant, pageable).map(ecoleMapper::toDto);
    }

    @Caching(
            evict = {
                    @CacheEvict(value = "ecolesList", allEntries = true),
                    @CacheEvict(value = "ecoles", key = "#id"),
                    @CacheEvict(value = "ecolesByNomEtudiant", key = "#ecoleDto.nomEtudiant"),
                    @CacheEvict(value = "etudiantsList", allEntries = true),
                    @CacheEvict(value = "etudiants", allEntries = true),
                    @CacheEvict(value = "projetsList", allEntries = true),
                    @CacheEvict(value = "projets", allEntries = true),
                    @CacheEvict(value = "adressesList", allEntries = true),
                    @CacheEvict(value = "adresses", allEntries = true)
            }
    )
    public EcoleDto save(EcoleDto ecoleDto) {
        Ecole ecole = ecoleMapper.toEntity(ecoleDto);
        ecole.getEtudiants().stream().forEach(e -> e.setEcole(ecole));
        return ecoleMapper.toDto(ecoleRepository.save(ecole));
    }
}
