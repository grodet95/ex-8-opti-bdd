package com.exo2.Exercice2.service;

import com.exo2.Exercice2.dto.EtudiantDto;
import com.exo2.Exercice2.entity.Etudiant;
import com.exo2.Exercice2.mapper.EtudiantMapper;
import com.exo2.Exercice2.repository.EtudiantRepository;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@AllArgsConstructor
public class EtudiantService {

    private final EtudiantRepository etudiantRepository;
    private final EtudiantMapper etudiantMapper;

    @Cacheable(value = "etudiantsList")
    public Page<EtudiantDto> findAll(Pageable pageable) {
        return etudiantRepository.findAll(pageable).map(etudiantMapper::toDto);
    }

    @Cacheable(value = "etudiants", key = "#id")
    public EtudiantDto findById(Long id) {
        return etudiantMapper.toDto(etudiantRepository.findById(id).orElse(null));
    }

    @Cacheable(value = "etudiantsByNomAndPrenom", key = "#nom + #prenom")
    public EtudiantDto findOneByNomAndPrenom(String nom, String prenom) {
        return etudiantMapper.toDto(etudiantRepository.findOneEtudiantByNomAndPrenom(nom, prenom).orElse(null));
    }

    @Caching(
            evict = {
                    @CacheEvict(value = "etudiantsList", allEntries = true),
                    @CacheEvict(value = "etudiants", key = "#id"),
                    @CacheEvict(value = "etudiantsByNomAndPrenom", key= "#etudiantDto.nom + #etudiantDto.prenom"),
                    @CacheEvict(value = "projetsList", allEntries = true),
                    @CacheEvict(value = "projets", allEntries = true)
            }
    )
    public EtudiantDto save(EtudiantDto etudiantDto) {
        return etudiantMapper.toDto(etudiantRepository.save(etudiantMapper.toEntity(etudiantDto)));
    }

    @Caching(
            evict = {
                    @CacheEvict(value = "etudiantsList", allEntries = true),
                    @CacheEvict(value = "etudiants", key = "#id"),
                    @CacheEvict(value = "etudiantsByNomAndPrenom", key= "#etudiantDto.nom + #etudiantDto.prenom"),
                    @CacheEvict(value = "projetsList", allEntries = true),
                    @CacheEvict(value = "projets", allEntries = true)
            }
    )
    public EtudiantDto update(Long id, EtudiantDto etudiantDto) {
        return etudiantRepository.findById(id)
                .map(existingEtudiant -> {
                    Etudiant etudiant = etudiantMapper.toEntity(etudiantDto);
                    etudiant.setId(id);
                    if (Objects.nonNull(existingEtudiant.getEcole())) {
                        etudiant.setEcole(existingEtudiant.getEcole());
                    }
                    if(Objects.nonNull(existingEtudiant.getProjets()) || existingEtudiant.getProjets().size() != 0) {
                        etudiant.setProjets(existingEtudiant.getProjets());
                    }
                    return etudiantMapper.toDto(etudiantRepository.save(etudiant));
                })
                .orElse(null);
    }

    @Caching(
            evict = {
                    @CacheEvict(value = "etudiantsList", allEntries = true),
                    @CacheEvict(value = "etudiants", key = "#id")
            }
    )
    public void delete(Long id) {
        etudiantRepository.deleteById(id);
    }
}
