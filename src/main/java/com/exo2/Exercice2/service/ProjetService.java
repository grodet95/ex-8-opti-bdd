package com.exo2.Exercice2.service;

import com.exo2.Exercice2.dto.EtudiantDto;
import com.exo2.Exercice2.dto.ProjetDto;
import com.exo2.Exercice2.mapper.EtudiantMapper;
import com.exo2.Exercice2.mapper.ProjetMapper;
import com.exo2.Exercice2.repository.ProjetRepository;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class ProjetService {
    private ProjetRepository projetRepository;
    private ProjetMapper projetMapper;
    private EtudiantMapper etudiantMapper;

    @Cacheable(value = "projetsList")
    public Page<ProjetDto> findAll(Pageable pageable) {
        return projetRepository.findAll(pageable).map(projetMapper::toDto);
    }

    @Cacheable(value = "projets", key = "#id")
    public ProjetDto findById(Long id) {
        return projetMapper.toDto(projetRepository.findById(id).get());
    }

    @Caching(
            put = {
                    @CachePut(value = "projets", key = "#projetDto.id"),
            },
            evict = {
                    @CacheEvict(value = "projetsList", allEntries = true)
            }
    )
    public ProjetDto save(ProjetDto projetDto) {
        return projetMapper.toDto(projetRepository.save(projetMapper.toEntity(projetDto)));
    }

    @Cacheable(value="etudiantsByProjet", key="#id")
    public Page<EtudiantDto> findEtudiantsByProjetId(Long id, Pageable pageable)
    {
        return projetRepository.findEtudiantsByProjetId(id, pageable).map(etudiantMapper::toDto);
    }

}
